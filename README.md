Pyxis Luris
========================

The "Pyxis Luris" is based on a reference application created to show how
to develop applications following the [Symfony Best Practices][1].

Requirements
------------

  * PHP 7.1.3 or higher;
  * PDO-SQLite PHP extension enabled;
  * and the [usual Symfony application requirements][2].

Installation
------------

Install PHP vendors first:

```bash
$ composer install
```

Deployment
------------

To deploy application to **production** server use the following command:

```bash
$ ./vendor/bin/dep deploy production
```
Notice that the last argument should be our hostname, specified in deploy.php file, which in our case is indeed **production**
Deploy process is managed by *deployer*.

Commands to work with database
------------
Database configuration is stored in `.env` file in ***DATABASE_URL***. Here are some most-commonly-used commands to work with DB :
* `php bin/console doctrine:database:drop --force` - Drop(delete) the database. If it doesn't exist will throw an exception.
* `php bin/console doctrine:database:create` - Creates the database. Will fall out with an exception if one exists. 
* `php bin/console doctrine:schema:create` - Creates schema(tables) for given databse.  
* `php bin/console doctrine:fixtures:load --no-interaction` - Populates the DB with example data.

Usage
-----

There's no need to configure anything to run the application. If you have
installed the [Symfony client][4] binary, run this command to run the built-in
web server and access the application in your browser at <http://localhost:8000>:

```bash
$ cd my_project/
$ symfony serve
```

If you don't have the Symfony client installed, run `php bin/console server:run`.
Alternatively, you can [configure a web server][3] like Nginx or Apache to run
the application.

[1]: https://symfony.com/doc/current/best_practices/index.html
[2]: https://symfony.com/doc/current/reference/requirements.html
[3]: https://symfony.com/doc/current/cookbook/configuration/web_server_configuration.html
[4]: https://symfony.com/download
[5]: https://github.com/symfony/webpack-encore
