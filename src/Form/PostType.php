<?php

namespace App\Form;

use A2lix\TranslationFormBundle\Form\Type\TranslationsType;
use App\Entity\Post;
use App\Form\Type\DateTimePickerType;
use App\Form\Type\TagsInputType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PostType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('translations', TranslationsType::class, [
                'label' => false,
                'fields' => [
                    'title' => [
                        'field_type' => TextType::class,
                        'label' => 'label.title',
                    ],
                    'summary' => [
                        'field_type' => TextareaType::class,
                        'help' => 'help.post_summary',
                        'label' => 'label.summary',
                    ],
                    'content' => [
                        'field_type' => TextareaType::class,
                        'attr' => ['rows' => 20],
                        'help' => 'help.post_content',
                        'label' => 'label.content',
                    ]
                ],
                'excluded_fields' => ['slug']
            ])
            ->add('publishedAt', DateTimePickerType::class, [
                'label' => 'label.published_at',
                'help' => 'help.post_publication',
            ])
            ->add('tags', TagsInputType::class, [
                'label' => 'label.tags',
                'required' => false,
            ])
            ->add('images', CollectionType::class, [
                'allow_add' => true,
                'entry_type' => ImageType::class,
                'by_reference' => false
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Post::class,
        ]);
    }
}
