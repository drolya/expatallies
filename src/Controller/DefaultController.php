<?php


namespace App\Controller;


use App\Entity\Post;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends AbstractController
{
    public function homepage(Request $request): Response
    {
        $posts = $this->getDoctrine()->getRepository(Post::class)->findBy([], [
            'publishedAt' => 'DESC'
        ], 3);

        return $this->render('default/homepage.html.twig', [
            'posts' => $posts
        ]);
    }
}
